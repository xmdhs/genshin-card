# genshin-card
能够显示原神的个人信息。

svg 代码来自 https://genshin-card.getloli.com/ 。

之所以重新写一遍是因为他这个的 github 仓库被封了，虽然弄到了别人备份的代码，也修改代码让其支持国际服。但是想改成 typescript。改了一点，然后想了想还不如直接用 Go 重写得了。

目前只支持国际服，也就是亚、欧、美、港澳台服。国服没注册账户没法测试也就没法支持。

为了避免被 github 封，就干脆不放在 github 上了嘛。

## 演示
![](https://files.xmdhs.com/genshin/detail/hoyolab/os_asia/rand/77289159)